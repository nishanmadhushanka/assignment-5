#include <stdio.h>

int profit(int tickpri)
{
    return revenue(tickpri)-cost(tickpri);
}
int revenue(int tickpri)
{
    return attend(tickpri)*tickpri;
}
int cost(int tickpri)
{
    return 500 + (3*attend(tickpri));
}
int attend(int tickpri)
{
    return 120+(20 * (15-tickpri)/5);
}

int main()
{
    int tickpri, maxprofit, bestpri;
    printf("ticket price\t No. of attendees\t profit\n");
    for (tickpri=0; tickpri<50; tickpri=tickpri+5)
    {
        printf("Rs.%d\t\t %d\t\t\t %d\n",tickpri, attend(tickpri), profit(tickpri));
    }
    for (tickpri=0; tickpri<50; tickpri=tickpri+5)
    {
        if (maxprofit<profit(tickpri))
        {
            maxprofit = profit(tickpri);
            bestpri = tickpri;
        }
    }
    printf("The most profitable ticket price is Rs.%d", bestpri);
    return 0;
}
